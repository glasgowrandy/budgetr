// UI Controler: Handles all UI element interactions and changes
var UIControler = (function () {
    // List of classes to access.
    var DOMStrings = {
        budgetMonth: '.budget_title-month',
        budgetValue: '.budget_value',
        budgetIncValue: '.budget_income-value',
        budgetExpValue: '.budget_expenses-value',
        budgetPercentage: '.budget_expenses-percentage',
        inputType: '.add_type',
        inputDescription: '.add_description',
        inputValue: '.add_value',
        inputButton: '.add_button',
        containerIncome: '.income_list',
        containerExpenses: '.expenses_list',
        container: '.container',
        itemPercentage: '.item_percentage'
    }

    var nodeListForEach = function (list, callback) {

        for (var i = 0; i < list.length; i++) {
            callback(list[i], i)
        }
    }

    // Public Methods
    return {

        // New Background
        randomBackground: function () {
            css = "linear-gradient(rgba(0, 0, 0, 0.05), rgba(0, 0, 0, 0.25)), url(/src/images/%image%.jpg)"

            newImage = 'image' + (Math.floor(Math.random() * 7) + 1)

            // New CSS Image from generated value
            newCSS = css.replace('%image%', newImage)

            // Update CSS
            document.getElementById('top').style.backgroundImage = newCSS

        },

        formatNumber: function (num, type) {

            var numSplit, int, decimal

            num = Math.abs(num)

            // 200 --> 200.00
            num = num.toFixed(2)

            numSplit = num.split('.')

            int = numSplit[0]

            decimal = numSplit[1]

            if (int.length > 3) {
                int.substr(0, int.length - 3) + ',' + int.substr(int.length - 3, int.length)
            }

            return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + decimal



        },

        // Gets input from the input fields.
        getInput: function () {
            return {
                type: document.querySelector(DOMStrings.inputType).value,
                description: document.querySelector(DOMStrings.inputDescription).value,
                value: parseFloat(document.querySelector(DOMStrings.inputValue).value)
            }
        },

        // Gets the current list of domstrings being used
        getDOMStrings: function () {
            return DOMStrings
        },

        // Adds the item to the UI list
        addItemList: function (obj, type) {

            var html, element, newhtml

            if (type === 'exp') {

                element = DOMStrings.containerExpenses

                html = ' <div class="item clearfix" id="exp-%id%">\
                            <div class="item_description">%description%</div>\
                            <div class="right clearfix">\
                                <div class="item_value">%value%</div>\
                                <div class="item_percentage">21%</div>\
                                <div class="item_delete">\
                                    <button class="item_delete-btn">\
                                        <i class="ion-ios-close-outline"></i >\
                                    </button >\
                                </div>\
                            </div>\
                        </div>'

            }
            else if (type === 'inc') {

                element = DOMStrings.containerIncome

                html = '<div class="item clearfix" id="inc-%id%">\
                            <div class="item_description">%description%</div>\
                            <div class="right clearfix">\
                                <div class="item_value">%value%</div>\
                                <div class="item_delete">\
                                     <button class="item_delete-btn">\
                                        <i class="ion-ios-close-outline"></i>\
                                    </button>\
                                </div>\
                            </div>\
                        </div>'
            }

            // Replace data in Placeholder
            newhtml = html.replace('%id%', obj.id)
            newhtml = newhtml.replace('%description%', obj.description)
            newhtml = newhtml.replace('%value%', this.formatNumber(obj.value, type))

            // Add new html
            document.querySelector(element).insertAdjacentHTML('beforeend', newhtml)

        },

        // Deletes Item from UI list
        deleteItemList: function (selectorID) {

            // the item object
            var element = document.getElementById(selectorID)

            // remove from parent object
            element.parentNode.removeChild(element)

        },

        // Sets the current month 
        updateDate: function () {
            var now, months

            // Get todays date
            now = new Date()

            // List of months
            months = [
                'January', 'February', 'March', 'April',
                'May', 'June', 'July', 'August',
                'September', 'October', 'November', 'December'
            ]
            // Updating the date
            document.querySelector(DOMStrings.budgetMonth).textContent = months[now.getMonth()] + ', ' + now.getFullYear()
        },

        // Displays the current budget
        updateBudget: function (object) {

            var type
            object.budget > 0 ? type = 'inc' : type = 'exp'

            // 1. Update Total Budget
            document.querySelector(DOMStrings.budgetValue).textContent = this.formatNumber(object.budget, type)

            // 2. Update Total Income
            document.querySelector(DOMStrings.budgetIncValue).textContent = '+ ' + object.totalInc

            // 3. Update Total Expenses
            document.querySelector(DOMStrings.budgetExpValue).textContent = '- ' + object.totalExp

            // 4. Update the percentage
            if (object.percentage > 0) {

                document.querySelector(DOMStrings.budgetPercentage).textContent = object.percentage + '%'

            } else {

                document.querySelector(DOMStrings.budgetPercentage).textContent = '--'

            }
        },

        // Clears the input fields
        clearField: function () {
            // Clear
            document.querySelector(DOMStrings.inputDescription).value = ''
            document.querySelector(DOMStrings.inputValue).value = ''

            // Focus
            document.querySelector(DOMStrings.inputDescription).focus()
        },

        // Calculates the total percentages for each item
        displayPercentages: function (percentages) {

            var fields = document.querySelectorAll(DOMStrings.itemPercentage)

            nodeListForEach(fields, function (cur, index) {

                if (percentages[index] > 0) {
                    cur.textContent = percentages[index] + '%'
                }
                else {
                    cur.textContent = '--'
                }
            })
        },

        // Changes the outline color
        changeType: function () {

            var fields = document.querySelectorAll(DOMStrings.inputType + ',' + DOMStrings.inputDescription + ',' + DOMStrings.inputValue)

            nodeListForEach(fields, function (cur) {
                cur.classList.toggle('red-focus')
            })

        }

    }
})();

// Budget Controller: Handles all of the datastructure manipulations and calculations
var budgetControler = (function () {

    // DS: Expense
    var Expense = function (id, description, value) {

        this.id = id
        this.description = description
        this.value = value
        // Default
        this.percentage = -1
    }

    Expense.prototype.calculatePercentage = function (totalInc) {
        if (totalInc > 0) {
            this.percentage = Math.round((this.value / totalInc) * 100)
        }
        else {
            this.percentage = -1
        }
    }
    Expense.prototype.getPercentage = function () {
        return this.percentage
    }

    // DS: Income
    var Income = function (id, description, value) {
        this.id = id
        this.description = description
        this.value = value
    }


    var data = {
        budget: 0,
        percentage: - 1,
        totals: {
            exp: 0,
            inc: 0
        },
        allItems: {
            exp: [],
            inc: []
        }
    }

    var calculateTotal = function (type) {

        var total = 0

        // 1. Sum up total
        data.allItems[type].forEach(function (cur) {
            total += cur.value
        })

        // 2. Set total
        data.totals[type] = total

    }

    // Public Methods
    return {

        // Returns the budget information
        getBudget: function () {
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            }
        },

        getPercentages: function () {
            var allPcts = data.allItems.exp.map(function (cur) {
                return cur.getPercentage()
            })

            return allPcts
        },

        addItem: function (type, des, val) {

            var newItem, id

            // Create a new id based on the last id in the array.
            if (data.allItems[type].length > 0) {
                id = data.allItems[type][data.allItems[type].length - 1].id + 1
            }
            else { id = 0 }

            if (type === 'exp') {
                newItem = new Expense(id, des, val)
            }
            else if (type === 'inc') {
                newItem = new Income(id, des, val)
            }

            // Add the newItem to its list.
            data.allItems[type].push(newItem)

            // Return the new item
            return newItem
        },

        deleteItem: function (type, id) {

            var ids, index

            // Map all of the itmes
            ids = data.allItems[type].map(function (cur) {
                return cur.id
            })

            // Get the index
            index = ids.indexOf(id)

            // Remove the item
            if (index !== -1) {
                data.allItems[type].splice(index, 1)
            }

        },


        calculateBudget: function () {
            // 1. Calculate the Income Total
            calculateTotal('inc')

            // 2. Calculate the Expense Total
            calculateTotal('exp')

            // 3. Calculate the Budget Total: inc - exp
            data.budget = data.totals.inc - data.totals.exp

            // 4. Calculate the percentage
            if (data.totals.inc > 0) {
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100)
            } else {
                data.percentage = -1
            }
        },

        calculatePercentages: function () {
            data.allItems.exp.forEach(function (cur) {
                return cur.calculatePercentage(data.totals.inc)
            })
        },

        // Used only for testing
        testing: function () {
            return data
        }


    }

})();

// Global App Controler: The Main interaction between the user and the functions
var appControler = (function (budgetCtrl, UICtrl) {

    var setEventListiners = function () {

        // List of DOM Elements
        var DOMStrings = UICtrl.getDOMStrings()

        // Add new income or expense on 'button'
        document.querySelector(DOMStrings.inputButton).addEventListener('click', ctrlAddItem)

        // Add new income or expense on 'enter'
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem()
            }
        })

        // Delete 'inc' or 'exp' item
        document.querySelector(DOMStrings.container).addEventListener('click', ctrlDeleteItem)

        // Change colors of outlines on change
        document.querySelector(DOMStrings.inputType).addEventListener('change', UICtrl.changeType)

    }

    var updateBudget = function () {
        // 1. Calculate Budget
        budgetCtrl.calculateBudget()

        // 2. Return the budget
        var budget = budgetCtrl.getBudget()

        // 3. Update Budget UI
        UICtrl.updateBudget(budget)
    }

    var updatePercentages = function (percentages) {

        // 1. Calculate Percents
        budgetCtrl.calculatePercentages()

        // 2. Read percents from the budget controler
        var percentages = budgetCtrl.getPercentages()

        // 3. Update UI with percents
        UICtrl.displayPercentages(percentages)
        console.log(percentages)
    }

    // Driver for adding an item 
    var ctrlAddItem = function () {

        var input, newItem

        // 1. Get Input
        input = UICtrl.getInput()
        // 2. Add Input to Data struct
        if (input.description !== '' && !isNaN(input.value) && input.value > 0) {

            // Add the item to the ds
            newItem = budgetCtrl.addItem(input.type, input.description, input.value)

            // Add the item to the UI
            UICtrl.addItemList(newItem, input.type)

            // Clear the fields
            UICtrl.clearField()

            // Update Budget
            updateBudget()

            // Update Percentage
            updatePercentages()


        }

    }

    var ctrlDeleteItem = function (event) {

        var itemID, type, splitID, id

        // Get container with the ID
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id

        if (itemID) {

            splitID = itemID.split('-')
            id = parseInt(splitID[1]) // ITEM ID
            type = splitID[0]   // ITEM Type

            // 1. Delete Item from DS
            budgetCtrl.deleteItem(type, id)

            // 2. Delete Item from UI
            UICtrl.deleteItemList(itemID)

            // 3. Update Budget and Percents
            updateBudget();
        }

    }
    return {
        init: function () {
            // Initial setups
            console.log('Application has started.')
            // 1. Randomly choose a new photo background
            UICtrl.randomBackground()

            // 2. Init Display budget
            UICtrl.updateBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: 0
            })

            // 3. Startup Event Listiners
            setEventListiners()
            // 4. Update Month
            UICtrl.updateDate()
        },
    }
})(budgetControler, UIControler);

// INIT
appControler.init();